(defmacro while (test &rest body)
  "Repeat body while test."
  (list* 'loop
         (list 'unless test '(return nil))
         body))

(while (< x 4) (setf x (+ x 1)) (print x))

(defun while2 (test &rest body)
  "Repeat body while test."
  (eval (list* 'loop
               (list 'unless test '(return nil))
               body)))

(while2 '(< x 4) '(setf x (+ x 1)) '(print x))

(defmacro while3 (test &rest body)
  "Repeat body while test."
  `(loop
      (unless ,test (return nil))
      ,@body))

(defun while4 (test &rest body)
  "Repeat body while test."
  (eval (list* 'loop
               (list 'unless test '(return nil))
               body)))

(defun negate-sym-list (list)
  (mapcar (lambda (x)
            (join-string- (symbol-name x)) list))


(defun combos (list)
  (if (null list) '(nil)
      (let* ((a (car list))
             (d (cdr list))
             (s (combos d))
             (v (mapcar (lambda (x) (cons a x)) s)))
        (append s v))))

(defun join-string-list (string-list)
    "Concatenates a list of strings
and puts spaces between the elements."
    (format nil "~{~A~^~}" string-list))

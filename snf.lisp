(in-package #:ltl)

(defparameter *temp-op* '(always next eventually))
(defparameter *log-conn* '(and or not => <=>))
(defparameter *log-quant* '(forall exists))
(defparameter *log-identities* '(true false))

;; Support function for formal manipulation

(defun rule-pattern (rule) (first rule))
(defun rule-responses (rule) (rest rule))

(defmacro and-first (p) `(cadr ,p))
(defmacro and-rest (p) `(caddr ,p))

(defun temp-op-p (p)
  "Return t if p is a temporal operator."
  (member p *temp-op*))

(defun log-conn-p (p)
  "Return t if p is a logical connector."
  (member p *log-conn*))

(defun log-quant-p (p)
  "Return t if p is a logical quantifier."
  (member p *log-quant*))

(defun log-id-p (p)
  "Return t if p is a logical identity."
  (member p *log-identities*))

(defun not-log-id-p (p)
  "Return not-log-id-p negated."
  (not (member p *log-identities*)))

(defun tmp-sym-p (p)
  "Return t if symbol name start with !."
  (if (equal (char (symbol-name p) 0) #\%) p))

(defun litteral-p (p)
  "Return t if p is a valid litteral"
  (cond ((and (symbolp p)
             (not (log-conn-p p))
             (not (log-quant-p p))
             ;;(not (log-id-p p))
             (not (temp-op-p p))
             (not (eq p 'start))))
        ((and
         (listp p)
         (equal (length p) 2)
         (eq (first p) 'not)
         (symbolp (second p))) t)
        (t nil)))

(defun not-litteral-p (p)
  "Negated litteral-p."
  (not (litteral-p p)))

(defun apply-rule (input rule-set)
  "Find some rule with which to transform the input."
  (some #'(lambda (rule)
            (let ((result (pat-match (rule-pattern rule) input)))
              (if (not (eq result fail))
                  ;; (progn
                  ;;   (print rule)
                    (sublis result
                            (random-elt (rule-responses rule))))));)
        rule-set))

(defun apply-and-replace (p rule-set)
  "After having applied the rules generate new symbols for new variables."
  (sublis
   (mapcar #'(lambda (x)
               (cons x (gensym (concatenate 'string "tmp-" (symbol-name x)))))
           (remove-duplicates
            (mapfilter #'tmp-sym-p (flatten-rec (apply-rule p rule-set)))))
   (apply-rule p rule-set)))

(defun generate-connector (conn-sym calling-pred &optional (obj-pred 'litteral-p))
  "Generate a generic set of rules for a recursive list o specified object
   connected by a certain connector."
  `(
    ((,conn-sym (?is ?A ,calling-pred) (?is ?B ,calling-pred)) ?A)
    ((,conn-sym (?is ?A ,calling-pred) (?is ?B ,obj-pred)) ?A)
    ((,conn-sym (?is ?A ,obj-pred) (?is ?B ,calling-pred)) ?A)
    ((,conn-sym (?is ?A ,obj-pred) (?is ?B ,obj-pred)) ?A)
    ((,conn-sym (?is ?A ,obj-pred) nil) ?A)
    ((,conn-sym (?is ?A ,obj-pred)) ?A)
    ))

(defparameter *snf-main-form*
  ;; Main form of a snf formula.
  '(((always (?is ?A ri-and-form-p)) t)))

(defun sym-and-form-p (p)
  "Return t if a list of symbols is in and form.
   Fisher pag 28."
  (or
   (and (listp p)
        (apply-and-replace p (generate-connector 'and 'sym-and-form-p 'litteral-p)))
   (litteral-p p)))

(defun ri-and-form-p (p)
  "Return t if a list of symbols is in and form.
   Fisher pag 28."
  (or
   (and (listp p)
    (apply-and-replace p (generate-connector 'and 'ri-and-form-p 'ri-form-p)))
   (litteral-p p)))

(defun sym-or-form-p (p)
  "Return t if a list of symbols is in or form.
   Fisher pag 28."
  (or
   (and (listp p)
    (apply-and-replace p (generate-connector 'or 'sym-or-form-p 'litteral-p)))
   (litteral-p p)))

(defparameter *ri-form*
  ;; form for rules Ri
  '(
    ((=> start (?is ?L sym-or-form-p)) t)
    ((=> (?is ?K sym-and-form-p) (next (?is ?L sym-or-form-p))) t)
    ((=> (?is ?K sym-and-form-p) (eventually (?is ?L litteral-p))) t)
    ))

(defun ri-form-p (p)
   "Return t if a formula is in snf form
    Fisher pag 28."
   (apply-and-replace p *ri-form*))

(defparameter *snf-main-form*
  ;; Main form of a snf formula.
  '(((always (?is ?A ri-and-form-p)) t)))

(defun snf-p (p)
   "Return t if a formula is in snf form
    Fisher pag 28."
   (apply-and-replace p *snf-main-form*))

;; TRANSFORMATION IN SNF FORM
;; Rules for separated normal form transormation.

(defparameter *anchor-rules*
  ;; Anchor A to the start, and rename.
  ;; Fisher pag 92 point 1.
  '(((?A)
     (and
      (=> start %Z)
      (=> %Z ?A)))))

(defparameter *snf-transf-rule-set*
  '(
    ((and ?L) ?L)
    ;; Remove any classical constructions on the right-hand side of clauses.
    ;; Fisher pag 92 point 2.
    ((=> ?L (and ?A ?B))
     (and
      (=> ?L ?A)
      (=> ?L ?B)))
    ((=> ?L (not (and ?A ?B)))
     (=> ?L (or (not ?A) (not ?B))))
    ((=> ?L (not (=> ?A ?B)))
     (and
      (=> ?L ?A)
      (=> ?L (not ?B))))

    ;; Rename any embedded formulae.
    ;; Fisher pag 92 point 3.
    ((=> ?L ((?is ?T1 temp-op-p) (?is ?NL not-litteral-p)))
     (and
      (=> ?L (?T1 %X))
      (=> %X ?NL)))
    ;; Process negations applied to temporal operators.
    ;; Fisher pag 92 point 4.
    ((=> ?L (not (next ?P)))
     (=> ?L (next (not ?P))))
    ((=> ?L (not (always ?Q)))
     (=> ?L (eventually (not ?Q))))
    ;; Remove unwanted temporal operators (and their negated forms).
    ;; Fisher pag 93 point 5.
    ;; ((=> ?L (always ?Q))
    ;;  (and
    ;;   (=> ?L %Z)
    ;;   (and
    ;;    (=> %Z ?Q)
    ;;    (and
    ;;     (=> %Z (next %Z))))))
    ((=> ?L (always ?Q))
     (and
      (=> %Z (next %Z))
      (and
       (=> start (or (not ?L) %Z))
       (and
        (=> true (next (or (not ?L) %Z)))
        (and
         (=> start (or (not %Z) ?Q))
         (=> true (next (or (not %Z) ?Q))))))))

    ((=> ?L (not (eventually ?R)))
     (and
      (=> ?L %W)
      (and
       (=> %W (not ?R))
       (and
        (=> %W (next %W))))))
    ;; Mystical rule Fisher pag 94 in the example :x
    ((=> ?X2 (=> ?P (next ?P)))
     (=> (and ?X2 ?P) (next ?P)))
    ;; Rewrite symple implications
    ((=> (?is ?A litteral-p) (?is ?B litteral-p))
     (and
      (=> start (or (not ?A) ?B))
      (=> true (next (or (not ?A) ?B)))))
    ))

(defparameter *snf-final-rewrite-rule-set*
  '(
    ;; Perform some final rewriting and simplification to get clauses into the exact
    ;; SNF form.
    ;; Fisher pag 93 point 6.
    ((=> ?L (or ?C (next ?R)))
     (=> (and (not ?C) ?L) (next ?R)))
    ((=> true ?R)
     (and
      (=> start ?R)
      (=> true (next ?R))))
    ((=> ?L (next false))
     (and
      (=> start (not ?L))
      (=> true (next (not ?L)))))
    ))

(defparameter *test-prop* '(and
                            (and
                             (eventually p)
                             (always (=> p (next p))))
                            (always (eventually (not p)))))
(defparameter *simple-test*
  '(always (eventually p)))

(defun transform-formula (p rules)
  "Apply a single transformation to a sentence taking it in the snf direction."
  (apply-and-replace p rules))

(defun transform-formula2 (p rules)
  "Apply a single transformation to a sentence taking it in the snf direction.
   Return the original prop if unable to perform the transformation."
  (let ((new-prop (apply-and-replace p rules)))
    (if new-prop new-prop p)))

(defun ->snf (p)
  "Convert a proposition in snf format. Fisher pag 92."
  (let ((prop (anchor p)))
    ;;(ri-eat (ri-eat prop *snf-transf-rule-set*) *snf-final-rewrite-rule-set*)))
    (cons 'always (cons (ri-eat prop *snf-transf-rule-set*) nil))))

(defun ri-eat (p rules)
  "Transform recursivelly all the Ri sentences"
  (if (listp p)
      (mapcar #'(lambda (x)
                  (if (ri-form-p x) x
                      (ri-eat x rules)))
              (transform-formula2 p rules))
      p))

(defun anchor (p)
  "Anchor A to the start, and rename.
   Fisher pag 92 point 1."
  (apply-and-replace (list p) *anchor-rules*))

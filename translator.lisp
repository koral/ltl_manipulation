(defmacro ltl-< (&rest program)
  `(print ',(ltl-convert program)))

(defun ltl-convert (sentence)
  (cond ((and
          (listp (first sentence))
          (eql (first (first sentence)) 'if)
          (equal (length (first sentence)) 4))
         (decode-if sentence))
       (t sentence)))

(defun decode-if (sentence)
  (let* ((if-sent (first sentence))
         (test (second if-sent))
         (s1 (third if-sent))
         (s2 (fourth if-sent))
         (s3 (second sentence)))
    (list 'and
          (list '=> test (ltl-convert (list s1 s3)))
          (list '=> (list 'not test) (ltl-convert (list s2 s3))))))

;; (ltl-< (if t (print "true") (print "false")) (print "false"))

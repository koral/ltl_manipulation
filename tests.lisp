(in-package #:ltl)

;; TESTS FOR SNF VERIFICATION

;; Litteral tests
(assert (litteral-p 'a))
(assert (not (litteral-p '(a))))
(assert (litteral-p '(not a)))
(assert (not (litteral-p '(not a b))))
(assert (not (litteral-p '(a b))))
;; Connected list tests
(assert (sym-and-form-p 'a))
(assert (sym-and-form-p '(and a b)))
(assert (not (sym-and-form-p '(and a b c))))
(assert (not (sym-and-form-p '(a and c))))
(assert (sym-and-form-p '(and a (and a b))))
(assert (not (sym-and-form-p '(and a b (and a b)))))
(assert (sym-and-form-p '(and a nil)))
(assert (sym-or-form-p '(or a b)))
(assert (not (sym-or-form-p '(or a b c))))
(assert (not (sym-or-form-p '(a or c))))
(assert (sym-or-form-p '(or a (or a b))))
(assert (not (sym-or-form-p '(or a b (or a b)))))
(assert (sym-or-form-p '(or a nil)))
;; Ri form tests
(assert (ri-form-p '(=> start (or a b))))
(assert (not (ri-form-p '(=> start (and a b)))))
(assert (ri-form-p '(=> (and a b) (next (or a b)))))
(assert (not (ri-form-p '(=> (and a b) (next (and a b))))))
(assert (not (ri-form-p '(=> (or a b) (next (or a b))))))
(assert (ri-form-p '(=> (and a b) (eventually g))))
(assert (not (ri-form-p '(=> (and a b) (eventually (and a b))))))
;; SNF form test
(assert (snf-p '(always
                 (and (=> (and a b) (eventually g))
                  (and (=> (and a b) (next (or a b)))
                   (=> start (or a b)))))))
;; Fisher pag 95.
(assert (snf-p '(always
                 (and (=> start x1)
                  (and
                   (=> x1 (eventually p))
                   (and
                    (=> x3 (next x3))
                    (and
                     (=> (and x2 p) (next p))
                     (and
                      (=> start (or (not x1) x3))
                      (and
                       (=> true (next (or (not x1) x3)))
                       (and
                        (=> start (or (not x3) x2))
                        (and
                         (=> true (next (or (not x3) x2)))
                         (and
                          (=> x4 (eventually (not p)))
                          (and
                           (=> x5 (next x5))
                           (and
                            (=> start (or (not x1) x5))
                            (and
                             (=> true (next (or (not x1) x5)))
                             (and
                              (=> start (or (not x5) x4))
                              (and
                               (=> true (next (or (not x5) x4)))
                               nil)))))))))))))))))


;; TESTS FOR TRANSFORMATIONS

;; Fisher point 2 pag 92.
(assert (pat-match '(AND (=> START ?X) (=> ?X A)) (anchor 'a)))

;; Fisher point 2 pag 92.
(assert (equal (transform-formula '(=> X (and Y Z)) *snf-transf-rule-set*)
               '(AND (=> X Y) (=> X Z))))
(assert (equal (transform-formula '(=> X (not (and Y Z))) *snf-transf-rule-set*)
               '(=> X (OR (NOT Y) (NOT Z)))))
(assert (equal (transform-formula '(=> X (not (=> Y Z))) *snf-transf-rule-set*)
        '(AND (=> X Y) (=> X (NOT Z)))))

;; Fisher point 3 pag 92.

(assert (pat-match '(AND (=> A (NEXT ?X)) (=> ?X (NEXT B)))
                   (transform-formula '(=> a (next (next b))) *snf-transf-rule-set*)))

(assert (not (transform-formula '(=> a (nexto (next b))) *snf-transf-rule-set*)))

;; Fisher point 4 pag 92.

(assert (equal (transform-formula '(=> L (not (next p))) *snf-transf-rule-set*)
               '(=> L (NEXT (NOT P)))))
(assert (equal (transform-formula '(=> L (not (always q))) *snf-transf-rule-set*)
               '(=> L (EVENTUALLY (NOT Q)))))

;; Fisher point 5 pag 93.

(assert (pat-match '(and
                     (=> ?X5 (next ?X5))
                     (and
                      (=> start (or (not ?X1) ?X5))
                      (and
                       (=> true (next (or (not ?X1) ?X5)))
                       (and
                        (=> start (or (not ?X5) ?X4))
                        (=> true (next (or (not ?X5) ?X4)))))))
                   (transform-formula '(=> x1 (always x4)) *snf-transf-rule-set*)))

(assert (pat-match '(AND (=> L ?W) (AND (=> ?W (NOT R)) (AND (=> ?W (NEXT ?W)))))
                   (transform-formula '(=> L (not (eventually r))) *snf-transf-rule-set*)))

(assert (pat-match '(=> (AND ?X ?P) (NEXT ?P))
                   (ri-eat '(=> A (=> B (NEXT B))) *snf-transf-rule-set*)))

;; (assert (equal (transform-formula '(=> L (or Y (next Z))))
;;               '(=> (AND (NOT Y) L) (NEXT Z))))
;; (assert (equal (transform-formula '(=> true X))
;;                '(AND (=> START X) (=> TRUE NEXT X))))
;; (assert (equal (transform-formula '(=> X (next false)))
;;                '(AND (=> START (NOT X)) (=> TRUE (NEXT (NOT X))))))
;; (assert (equal (transform-formula '(=> x1 x2))
;;                '(AND (=> START (OR (NOT X1) X2)) (=> TRUE (NEXT (OR (NOT X1) X2))))))

(assert (ri-and-form-p (ri-eat '(=> x1 (always x4)) *snf-transf-rule-set*)))
(assert (ri-and-form-p (ri-eat '(=> x1 (always (eventually (not p)))) *snf-transf-rule-set*)))
(assert (snf-p (->snf *test-prop*)))

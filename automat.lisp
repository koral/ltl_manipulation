(load "aux-fns.lisp")
(load "graph.lisp")

;; automaton definition
(defstruct automaton
  name
  alphabet
  states
  transitions
  initial-states
  final-states)

(defun get-init (trans)
  "Get the initial state from a transition."
  (first trans))

(defun get-cond (trans)
  "Get the condition from a transition."
  (second trans))

(defun get-final (trans)
  "Get the final state from a transition."
  (third trans))

(defun join-trans (trans1 trans2)
  "Join two transitions."
  (list (list (get-init trans1) (get-init trans2))
        (join-two-symbol (get-cond trans1) (get-cond trans2))
        (list (get-final trans1) (get-final trans2))))

(defun product-automata (aut1 aut2)
  "Return the cross products between two automata."
  (let* ((new-states (cross-product #'list
                          (automaton-states aut1)
                          (automaton-states aut2))))
    (make-automaton
     :name (concatenate 'string (automaton-name aut1) (automaton-name aut2))
     :alphabet (union (automaton-alphabet aut1) (automaton-alphabet aut2))
     :states new-states
     :transitions (product-transitions aut1 aut2)
     :initial-states (flatten (cross-product #'list (automaton-initial-states aut1)
                                             (automaton-initial-states aut2)))
     :final-states  (flatten (cross-product #'list (automaton-final-states aut1)
                                    (automaton-final-states aut2))))))

(defun product-transitions (aut1 aut2)
  "Implementation of the transitions cross product (Fisher pag.35).
   Return the transitions for the new automaton."
  (let ((alphabet (intersection (automaton-alphabet aut1) (automaton-alphabet aut2))))
    (remove nil
            (mapcar #'(lambda (x)
                        (if (and (alph-member (get-cond (first x)) alphabet)
                                 (alph-member (get-cond (second x)) alphabet))
                            (join-trans (first x) (second x))))
                    (cross-product #'list (automaton-transitions aut1)
                                   (automaton-transitions aut2))))))

(defun reached-by (tran trans)
  "Return if a transition is reached by at least another transition."
  (find-if #'(lambda (x)
               (when (equal (get-final x) (get-init tran)) t))
           trans))

;; To optimize!!
(defun purge-automaton (aut)
  "Iterate prune-automaton till reaching a purged automaton."
  (let ((old-trans nil))
    (loop do
         (setq old-trans (copy-list (automaton-transitions aut)))
         (setq aut (prune-automaton aut))
       while (not (equal (automaton-transitions aut) old-trans))))
  aut)

(defun prune-automaton (aut)
  "Modify the automaton (and return it) with the transition list pruned of one level for
   unreachable states."
  (setf (automaton-transitions aut)
        (mapfilter #'(lambda (x)
                       (when (or (reached-by x (automaton-transitions aut))
                                 (equal (get-init x) (automaton-initial-states aut))) x))
                   (automaton-transitions aut)))
  aut)

(defparameter aut1
  (make-automaton :name "always a"
                  :alphabet '(a b)
                  :states '(s1 s0)
                  :transitions '((s1 a s0) (s0 a s0))
                  :initial-states '(s1)
                  :final-states '(s0)))

(defparameter aut2
  (make-automaton :name "always b"
                  :alphabet '(a b)
                  :states '(t1 t0)
                  :transitions '((t1 b t0) (t0 b t0))
                  :initial-states '(t1)
                  :final-states '(t0)))

;; (automaton->png "graph" aut1)
;; (automaton->png "graph" (product-automata aut1 aut2))

(defparameter aut3
  (make-automaton :name "(next a) ∨ (next b)"
                  :alphabet '(a b x y)
                  :states '(si s1 s2 s3)
                  :transitions '((si nil s1) (si nil s2) (s1 a s3) (s2 b s3) (s3 nil s3))
                  :initial-states '(si)
                  :final-states '(s3)))

(defparameter aut4
  (make-automaton :name "(x ∧ next y) ∨ (y ∧ next x)"
                  :alphabet '(a b x y)
                  :states '(ti t1 t2 t3)
                  :transitions '((ti x t1) (ti y t2) (t1 y t3) (t2 x t3) (t3 nil t3))
                  :initial-states '(ti)
                  :final-states '(t3)))

;;(automaton->png "graph" (product-automata aut3 aut4))
;;(automaton->png "graph" (purge-automaton (product-automata aut3 aut4)))

(defparameter aut5
  (make-automaton :name "foo1"
                  :alphabet '(a)
                  :states '(s t)
                  :transitions '((s a t) (t a s))
                  :initial-states '(s)
                  :final-states '(t)))

(defparameter aut6
  (make-automaton :name "foo2"
                  :alphabet '(a)
                  :states '(s t)
                  :transitions '((t a s) (s a t))
                  :initial-states '(t)
                  :final-states '(s)))

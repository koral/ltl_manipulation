(in-package #:ltl)
(use-package :cl-ppcre)

(defparameter str
"DRA v2 explicit
Comment: \"Created by LTL3DRA v.0.1.1\"
States: 2
Acceptance-Pairs: 1
Start: 0
AP: 2 \"a\" \"b\"
---
State: 0
Acc-Sig:  +0
0
1
0
0
State: 1
Acc-Sig:  -0
1
1
1
1
")

(defstruct rab-aut
  n-states
  trans-list
  trans-val)

(defparameter test '("a" "b" "c"))

(defun join-string (string1 string2)
  "Concatenates two strings"
  (format nil "~{~A~^~}" (list string1 string2)))

(defun negate-sym-list (list)
  (mapcar (lambda (x)
            (join-string "!" x)) list))

(defun create-nil-list (len)
  "Return a len lenght list of nil elements."
  (loop repeat len append '(nil)))

(defun bit-set (n)
  "Return a list og  t or nil elements rappresenting n in two base."
  (mapcar (lambda (x)
            (if (equal #\1 x) t nil))
          (coerce (write-to-string n :base 2) 'list)))

(defun bit-set-len (n len)
  "As bit set but and nil alements in fron to reach len lenght and revers the
   order of the result.
   So first bit are the less significant."
  (reverse (let* ((l (bit-set n))
                  (len-new (length l)))
             (if (< len-new len)
                 (flatten (list (create-nil-list (- len len-new)) l))
                 l))))

(defun gen-trans-list (list)
  "Generate a list with all the transition bit ordered."
  (loop for i below (expt 2 (length list)) collect
       (mapcar (lambda (x y z)
                 (if x y z))
               (bit-set-len i (length list)) list (negate-sym-list list))))

(defun get-states-num (str)
  (parse-integer (scan-to-strings "States: [0-9]+" str) :start 8))

(defun get-variables (str)
  (cddr (split "\\s+" (scan-to-strings "AP: [0-9a-z ]+" (remove #\" str)))))

(defun decode-state-list (list)
  (remove nil (mapcar (lambda (x) (unless (or (search "State:" x) (search "Acc-Sig:" x))
                                       (parse-integer x))) list)))

(defun decode-cond-list (string-list)
  "Concatenates a list of strings
   and puts && between the elements."
  (format nil "~{~A~^ && ~}" string-list))

 (defun get-automata (str)
   (let ((str-splitted (split "\\n+" str)))
     (let ((n-states (get-states-num (elt str-splitted 2)))
           (trans-list (gen-trans-list (get-variables (elt str-splitted 5)))))
       (make-rab-aut
        :n-states n-states
        :trans-list trans-list
        :trans-val (decode-state-list (cdddr (cddddr str-splitted)))))))

(defun rab->dot (rab)
  (princ "Digraph {")
  (fresh-line)
  (loop for i below (rab-aut-n-states rab) do
       (loop for j below (length (rab-aut-trans-list rab)) do
            (format t "S~D->S~D [label=~s];~1%"
                    i
                    (nth j (rab-aut-trans-val rab))
                    (decode-cond-list (nth j (rab-aut-trans-list rab))))))
  (princ "}"))

(defun rabin->png (fname rab)
  (dot->png fname
            (lambda ()
              (rab->dot rab))))

(defun ltl3dra-wrap (formula)
  (rabin->png "graph.dot" (get-automata
                           (let ((*default-pathname-defaults*
                                  (make-pathname :directory (pathname-directory (pathname "formula.ltl")))))
                             (write-file "formula.ltl" formula)
    (trivial-shell:shell-command "./ltl3dra -F formula.ltl")))))

(in-package #:ltl)

;;;; AUXILIARY FUNCTIONS

(defun mappend (fn list)
  "Append the results of calling fn on each element of list.
   Like mapcon, but uses append instead of nconc."
  (apply #'append (mapcar fn list)))

(defun cross-product (fn xlist ylist)
  "Return a list of all (fb x y) values"
  (mappend #'(lambda (y)
               (mapcar #'(lambda (x) (funcall fn x y))
                       xlist))
           ylist))

(defun range (max &key (min 0) (step 1))
  "Return a list of natural number."
  (loop for n from min below max by step
     collect n))

(defun symbol-append (&rest symbols)
  "Append a list o symbol removing nil"
  (intern (apply #'concatenate 'string
                 (mapcar #'symbol-name symbols))))

(defun join-two-symbol (sym1 sym2)
  "Join two symbol. If one is nil the other is returned"
  (intern (cond
            ((eql sym1 sym2) (symbol-name sym1))
            ((eql sym1 nil) (symbol-name sym2))
            ((eql sym2 nil) (symbol-name sym1))
            (t (concatenate 'string (symbol-name sym1) (symbol-name sym2))))))

(defun alph-member (x alphabet)
  "Check if x is a member of the alphabet.
   Nil is always considered a member."
  (if (eql x nil) alphabet
      (member x alphabet)))

(defun mapfilter (fn list)
  "Like mapcar but doesn't collect nil results"
  (mapcan #'(lambda (x) (when (funcall fn x) (list x))) list))

(defun mklist (x)
  "If x is a list return it, otherwise return the list of x"
  (if (listp x) x (list x)))

(defun flatten (exp)
  "Get rid of imbedded lists (to one level only)."
  (mappend #'mklist exp))

(defun flatten-rec (obj)
  "Get rid of imbedded lists."
  (let (result)
    (labels ((grep (obj)
               (cond ((null obj))
                     ((atom obj) (push obj result))
                     (t (grep (rest obj))
                        (grep (first obj))))))
      (grep obj)
      result)))

(defun starts-with (list x)
  "Is x a list whose first element is x?"
  (and (consp list) (eql (first list) x)))

(defun starts-with-rec (list x)
  "Check if a list and every sublist in it begin with and."
  (if (equal x (first list))
      (and
       (every #'identity (mapcar #'(lambda (x)
                                     (if (listp x)
                                         (and-list-p x)
                                         t))
                                 (rest list))))))

(defun random-elt (seq)
  "Pick a random element out of a sequence."
  (elt seq (random (length seq))))

(defun true-p (x)
  "A predicate that simply return always true."
  t)

(defun write-file (name content)
  "Write a string to a file."
  (with-open-file (stream name
                          :direction :output
                          :if-exists :overwrite
                          :if-does-not-exist :create)
    (format stream content)))

(defun program-stream (program &optional args)
  (let ((process (sb-ext:run-program program args
                                     :input :stream
                                     :output :stream
                                     :wait nil
                                     :search t)))
    (when process
      (make-two-way-stream (sb-ext:process-output process)
                           (sb-ext:process-input process)))))

(defun slurp-stream4 (stream)
  (let ((seq (make-string (file-length stream))))
    (read-sequence seq stream)
    seq))

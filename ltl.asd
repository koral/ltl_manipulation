;;;; ltl.asd

(asdf:defsystem #:ltl
  :serial t
  :depends-on (#:cl-ppcre #:trivial-shell)
  :description "Library for operations on temporal logic programs"
  :author "Andrea Corallo <acorallo@lely.com>"
  :license "Specify license here"
  :components ((:file "package")
               (:file "ltl")
               (:file "aux-fns")
               (:file "pat-match")
               (:file "snf")
               (:file "graph")
               (:file "rabin")
               (:file "tests")))
